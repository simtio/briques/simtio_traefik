# Exemple docker-compose
```yaml
version: '3'
services:
  minio:
    image: minio/minio
    environment:
      - MINIO_ACCESS_KEY=test
      - MINIO_SECRET_KEY=testtest
    labels:
      - traefik.http.routers.my-app.rule=Host(`test.127.0.0.1.xip.io`)
      - traefik.http.services.my-app.loadbalancer.server.port=9000
      - traefik.docker.network=plugins_traefik_default
    command: server /data
    networks:
      - plugins_traefik_default
      - default
networks:
  plugins_traefik_default:
    external:
      name: plugins_traefik_default
```

# Récupérer le nom du réseau traefik:
```python
from core.config import Config
Config().get('plugins')['traefik']['network']
```