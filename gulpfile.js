const gulp = require('gulp');
const tar = require('gulp-tar-path');
const gzip = require('gulp-gzip');
const clean = require('gulp-clean');
const package = require('./package.json');

gulp.task('build', () =>
  gulp.src(['locales', 'views', 'docker-compose.yml', 'main.py'], {allowEmpty: true})
    .pipe(tar(package.name + '_' + package.version + '.tar'))
    .pipe(gzip())
    .pipe(gulp.dest('dist'))
);

gulp.task('clean', function () {
  return gulp.src('dist', {read: false})
    .pipe(clean());
});
