from core import BasePlugin


class Plugin(BasePlugin):
    def __init__(self):
        super(Plugin, self).__init__()

    def install(self):
        self.config.set("network", "plugins_traefik_default")
        self.deploy()
