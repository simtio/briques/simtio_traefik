#!/usr/bin/env bash

# Get out if variable is not initialized
set -e

############
## CONFIG ##
############
echo "
--------------------------------------------
  ___   ___   __  __   _____   ___    ___
 / __| |_ _| |  \/  | |_   _| |_ _|  / _ \
 \__ \  | |  | |\/| |   | |    | |  | (_) |
 |___/ |___| |_|  |_|   |_|   |___|  \___/

--------------------------------------------

Brought to you by SIMTIO
https://www.simtio.fr
--------------------------------------------


  _____ ____      _    _____ _____ ___ _  __
 |_   _|  _ \    / \  | ____|  ___|_ _| |/ /
   | | | |_) |  / _ \ |  _| | |_   | || ' /
   | | |  _ <  / ___ \| |___|  _|  | || . \
   |_| |_| \_\/_/   \_\_____|_|   |___|_|\_\



--------------------------------------------"

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
    set -- traefik "$@"
fi

# if our command is a valid Traefik subcommand, let's invoke it through Traefik instead
# (this allows for "docker run traefik version", etc)
if traefik "$1" --help >/dev/null 2>&1
then
    set -- traefik "$@"
else
    echo "= '$1' is not a Traefik command: assuming shell execution." 1>&2
fi

exec "$@"
